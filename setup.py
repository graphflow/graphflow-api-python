from distutils.core import setup

setup(
    name='graphflow-api',
    version='1.2.0',
    author='Nick Pentreath',
    author_email='nick@graphflow.com',
    packages=['graphflow'],
    url='https://bitbucket.org/graphflow/graphflow-api-python',
    license='LICENSE',
    description="Python client for Graphflow APIs",
    long_description=open('README').read(),
    package_data={'graphflow': [ 'models/*.py' ]},
    install_requires=[
    ],
)
