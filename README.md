Python Client for Graphflow's APIs

Generated using the Swagger codegen library.

To install using pip: ```pip install https://bitbucket.org/graphflow/graphflow-api-python/downloads/graphflow-api-1.0.3.tar.gz```

To install from source: ```python setup.py install```

The API consists of a ```ApiClient```, a ```UserApi```, ```ItemApi```, ```RecommendApi``` and ```AnalyticsApi```.

# Example Usage

```python
In [1]: from graphflow import swagger, UserApi, ItemApi, RecommendApi, AnalyticsApi

In [2]: clientKey = "YourClientKey"

In [3]: apiKey = "YourApiKey"

In [4]: apiServer="https://api.graphflow.com"

In [5]: client = swagger.ApiClient(clientKey=clientKey, apiKey=apiKey, apiServer=apiServe
r)

In [6]: userApi = UserApi.UserApi(client)

In [7]: itemApi = ItemApi.ItemApi(client)

In [8]: recommendApi = RecommendApi.RecommendApi(client)

In [9]: analyticsApi = AnalyticsApi.AnalyticsApi(client)

In [10]: userApi.
userApi.aliasUser            userApi.getInteractions      userApi.postInteractionList
userApi.apiClient            userApi.getUserById          userApi.postUser
userApi.deleteUserById       userApi.postInteraction      userApi.putUser

In [10]: itemApi.
itemApi.apiClient            itemApi.getItemById          itemApi.putItem
itemApi.deleteItemById       itemApi.getSimilarItems      itemApi.putItemActiveToggle
itemApi.getInteractions      itemApi.postItem

In [10]: recommendApi.
recommendApi.apiClient                   recommendApi.getRecommendationsUserItem
recommendApi.getRecommendations          recommendApi.getSimilarItems

In [10]: analyticsApi.
analyticsApi.apiClient             analyticsApi.getItemSearch
analyticsApi.getInteractionCount   analyticsApi.getUserActivity
analyticsApi.getInteractionSearch  analyticsApi.getUserCount
analyticsApi.getItemActivity       analyticsApi.getUserSearch
analyticsApi.getItemCount
```