#!/usr/bin/env python
"""
WordAPI.py
Copyright 2012 Wordnik, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

NOTE: This class is auto generated by the swagger code generator program. Do not edit the class manually.
"""
import sys
import os

from models import *


class ItemApi(object):

    def __init__(self, apiClient):
      self.apiClient = apiClient

    
    def putItem(self, body, **kwargs):
        """Update an item

        Args:
            body, Item: JSON item consisting of fields 'itemId' and 'itemData' (required)
            
        Returns: ApiResponse
        """

        allParams = ['body']

        params = locals()

        returnJSON = False
        if 'returnJSON' in params['kwargs']:
            if params['kwargs']['returnJSON'] == True:
                returnJSON = True
            params['kwargs'].pop('returnJSON')

        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method putItem" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/item/'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'PUT'

        queryParams = {}
        headerParams = {}

        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None
        if returnJSON:
            return response

        responseObject = self.apiClient.deserialize(response, 'ApiResponse')
        return responseObject
        
        
    def putItemActiveToggle(self, itemId, **kwargs):
        """Update item active status

        Args:
            itemId, str: ID of item (required)
            active, bool: Set 'active' flag to this value (default: true) (optional)
            
        Returns: ApiResponse
        """

        allParams = ['itemId', 'active']

        params = locals()

        returnJSON = False
        if 'returnJSON' in params['kwargs']:
            if params['kwargs']['returnJSON'] == True:
                returnJSON = True
            params['kwargs'].pop('returnJSON')

        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method putItemActiveToggle" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/item/activetoggle'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'PUT'

        queryParams = {}
        headerParams = {}

        if ('itemId' in params):
            queryParams['itemId'] = params['itemId']
        if ('active' in params):
            queryParams['active'] = params['active']
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None
        if returnJSON:
            return response

        responseObject = self.apiClient.deserialize(response, 'ApiResponse')
        return responseObject
        
        
    def getInteractions(self, itemId, **kwargs):
        """Get interactions for an item

        Args:
            itemId, str: ID of item (required)
            type, str: 
The type of the interaction (Default = ALL). Can be multi-valued, hence
&lt;pre&gt;?itemId=1&amp;type=view&amp;type=purchase&lt;/pre&gt; will retrieve both 'view' and 'purchase'
interactions
 (optional)
            num, int: Number of interactions to return. (Default: 20) (optional)
            withDetails, bool: Whether to include user and item data in response (Default: false) (optional)
            
        Returns: list[Interaction]
        """

        allParams = ['itemId', 'type', 'num', 'withDetails']

        params = locals()

        returnJSON = False
        if 'returnJSON' in params['kwargs']:
            if params['kwargs']['returnJSON'] == True:
                returnJSON = True
            params['kwargs'].pop('returnJSON')

        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method getInteractions" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/item/interaction'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'GET'

        queryParams = {}
        headerParams = {}

        if ('itemId' in params):
            queryParams['itemId'] = params['itemId']
        if ('type' in params):
            queryParams['type'] = params['type']
        if ('num' in params):
            queryParams['num'] = params['num']
        if ('withDetails' in params):
            queryParams['withDetails'] = params['withDetails']
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None
        if returnJSON:
            return response

        responseObject = self.apiClient.deserialize(response, 'list[Interaction]')
        return responseObject
        
        
    def putItemList(self, body, **kwargs):
        """Update multiple items

        Args:
            body, list: JSON list of items, each item consisting of fields 'itemId' and 'itemData' (required)
            
        Returns: ApiResponse
        """

        allParams = ['body']

        params = locals()

        returnJSON = False
        if 'returnJSON' in params['kwargs']:
            if params['kwargs']['returnJSON'] == True:
                returnJSON = True
            params['kwargs'].pop('returnJSON')

        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method putItemList" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/item/itemlist'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'PUT'

        queryParams = {}
        headerParams = {}

        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None
        if returnJSON:
            return response

        responseObject = self.apiClient.deserialize(response, 'ApiResponse')
        return responseObject
        
        
    def getItemById(self, itemId, **kwargs):
        """Get an item by ID

        Args:
            itemId, str: ID of item (required)
            
        Returns: Item
        """

        allParams = ['itemId']

        params = locals()

        returnJSON = False
        if 'returnJSON' in params['kwargs']:
            if params['kwargs']['returnJSON'] == True:
                returnJSON = True
            params['kwargs'].pop('returnJSON')

        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method getItemById" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/item/{itemId}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'GET'

        queryParams = {}
        headerParams = {}

        if ('itemId' in params):
            replacement = str(self.apiClient.toPathValue(params['itemId']))
            resourcePath = resourcePath.replace('{' + 'itemId' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None
        if returnJSON:
            return response

        responseObject = self.apiClient.deserialize(response, 'Item')
        return responseObject
        
        
    def deleteItemById(self, itemId, **kwargs):
        """Mark an item as inactive (default) or delete an item

        Args:
            itemId, str: ID of item (required)
            hardDelete, bool: Whether to hard delete the item (default: false) (optional)
            
        Returns: ApiResponse
        """

        allParams = ['itemId', 'hardDelete']

        params = locals()

        returnJSON = False
        if 'returnJSON' in params['kwargs']:
            if params['kwargs']['returnJSON'] == True:
                returnJSON = True
            params['kwargs'].pop('returnJSON')

        for (key, val) in params['kwargs'].iteritems():
            if key not in allParams:
                raise TypeError("Got an unexpected keyword argument '%s' to method deleteItemById" % key)
            params[key] = val
        del params['kwargs']

        resourcePath = '/item/{itemId}'
        resourcePath = resourcePath.replace('{format}', 'json')
        method = 'DELETE'

        queryParams = {}
        headerParams = {}

        if ('hardDelete' in params):
            queryParams['hardDelete'] = params['hardDelete']
        if ('itemId' in params):
            replacement = str(self.apiClient.toPathValue(params['itemId']))
            resourcePath = resourcePath.replace('{' + 'itemId' + '}',
                                                replacement)
        postData = (params['body'] if 'body' in params else None)

        response = self.apiClient.callAPI(resourcePath, method, queryParams,
                                          postData, headerParams)

        if not response:
            return None
        if returnJSON:
            return response

        responseObject = self.apiClient.deserialize(response, 'ApiResponse')
        return responseObject
        
        
    


